## sdm660_64-user 10 QKQ1.191014.001 eng.root.20201016.161857 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: lineage_RMX1801-userdebug
- Release Version: 10
- Id: QQ3A.200805.001
- Incremental: 10035469
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/RMX1801/RMX1801:10/QKQ1.191014.001/1602573502:user/release-keys
- OTA version: 
- Branch: sdm660_64-user-10-QKQ1.191014.001-eng.root.20201016.161857-release-keys
- Repo: oppo_rmx1801_dump_18816


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
