#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:23212032:b895c3275731e8c64b463bbfb1be17c66b760c81; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:13172736:8bd83c2457af40344dc07b9d74f482087ae0e85b \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:23212032:b895c3275731e8c64b463bbfb1be17c66b760c81 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
